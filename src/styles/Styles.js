import { css, keyframes } from "@emotion/react";

class Styles {
	constructor() {}
	
	Container = css`
		margin: auto auto;
	`;

	HFlex = css`
		display: flex;
		flex-direction: row;
		justify-content: center;
		align-items: center;
		flex-wrap: wrap;
	`;

	VFlex = css`
		display: flex;
		flex-direction: column;
		justify-content: center;
		align-items: center;
	`;


	Grid = css`
		column-count: 4;
		@media (max-width: 768px) {
			display: flex;
			flex-direction: column;
			justify-content: center;
			align-items: center;
		}
		@media (min-width: 769px) and (max-width: 888px) {
			column-count: 2;
		}
		@media (min-width: 889px) and (max-width: 1199px) {
			column-count: 3:
		}
	`;

	SlideUp () {
		const slideUp = keyframes`
			from {
				opacity: 0;
				margin-top: 15em;
			}

			to {
				opacity: 1;
				margin-top: 0em;
			}
		`;

		return css `
			-web-kit-animation: ${slideUp} 2s forwards;
			-moz-animation: ${slideUp} 2s forwards;
			-o-animation: ${slideUp} 2s forwards;
			animation: ${slideUp} 2s forwards;
		`
	};

	Input = css`
		color: #000000;
		background-color: #FFFFFF;
		width: 25em;
		line-height: 2.5em;
		border: none;
		&:focus {
			outline: none;
			border-bottom: 1px solid #000000;
			-webkit-transition: all ease-in-out .15s;
			-o-transition: all ease-in-out .15s;
			transition: all ease-in-out .s;
		}
	`;

	Button = css`
		background-color: #FFFFFF;
		line-height: 2.5em;
		width: 15em;
		margin: 0 0.5em 0 0.5em;
		border: 1px solid #000000;
		border-radius: 10px;
		&:hover {
			zoom: 1;
			filter: alpha(opacity=50);
			opacity: 0.5;
			-webkit-transition: opacity 1s ease-in-out;
			-moz-transition: opacity 1s ease-in-out;
			-ms-transition: opacity 1s ease-in-out;
			-o-transition: opacity 1s ease-in-out;
			transition: opacity 1s ease-in-out;
		}
		&:disabled {
			opacity: 0.7;
		}
		@media (max-width: 768px) {
			width: 25em;
			margin: 1em 0 0 0;
		}
		@media (min-width: 769px) and (max-width: 888px) {
			width: 20em;
			margin: 0 0.5em 0 0.5em;
		}
	`;

	Card = css`
		margin: auto;
		max-width: 100%;
	`;

	CardImage = css`
		max-width: 100%;
		border-radius: 1em;
	`;
}

export default Styles;