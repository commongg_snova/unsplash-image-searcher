/** @jsxImportSource @emotion/react */
import { useEffect, useState } from "react";
import axios from "axios";

import cx from "@emotion/css";
import { css } from "@emotion/react";
import InfiniteScroll from "react-infinite-scroll-component";

function PhotoSearch (props) {
	const [search, setSearch] = useState("");
	const [page, setPage] = useState(1);
	const [photos, setPhotos] = useState([]);

	const client_id="q57g9KZrGdETle39joMJDscIejPdMZXodFfRBTstgdg";
	const fetchUrl = `https://api.unsplash.com/search/photos?client_id=${client_id}&query=${search}&page=${page}&per_page=50`;

	const [opacity, setOpacity] = useState(1);

	const searchContainerBG = css`
		position: sticky;
		min-height; 10em;
		margin: auto auto;
		top: 0;
		z-index: 10;
		padding: 1em 0 1em 0;
		background-size: 100vw 10em;
		background-color: rgba(255, 255, 255, ${1 - opacity});
		animation: fade 2s linear;
	`
	
	useEffect(() => {
		window.onscroll = () => {
			const scrollHeight = Math.ceil(window.scrollY / 100) * 100;
			const opacity = Math.min(100 / scrollHeight, 1);
			setOpacity(opacity);
		}
	}, [])

	const callApi = () => {
		axios.get(fetchUrl, {
			headers: {}
		})
		.then((res) => {
			setPhotos([...photos, ...res.data.results]);
		})
		.catch((err) => {
			console.log(err);
		})
		setPage(page + 1);
	}

	const searchUnsplash = (e) => {
		e.preventDefault();
		callApi();
	}

	return (
		<div>
			<div css={cx(searchContainerBG)}>
			<form css={cx(props.Theme.HFlex, props.Theme.SlideUp())} onSubmit={searchUnsplash}>
				<div>
					<input
						type="text"
						placeholder="Search for an image tag on unsplash..."
						css={props.Theme.Input}
						value={search}
						onChange={(e) => {
							setSearch(e.target.value)
							setPhotos([]);
							setPage(1);
						}}
					/>
				</div>
				<div>
					<button
						type="submit"
						css={props.Theme.Button}
						>
						Search
					</button>
				</div>
			</form>
			</div>
			<InfiniteScroll
        dataLength={photos.length}
        next={callApi}
        hasMore={true}
        loader={
					<div>
					{
						photos.length > 0 
						? 
							<div css={props.Theme.HFlex}>
								Continue scrolling to load more iamges.
							</div>
					 :
					 		<div css={props.Theme.HFlex}>
								You have not searched for any images.
							</div>
					}
					</div>
				}
        endMessage={
          <div css={props.Theme.HFlex}>
            You have reached the end of your image search.
          </div>
        }
      >
				<div css={cx(props.Theme.Grid)}>
				{photos.map((photo, key) =>
					<div key={key} css={props.Theme.Card}>
							<img
								css={props.Theme.CardImage}
								key={key}
								alt={photo.alt_description}
								src={photo.urls.small}
							></img>
					</div>)
				}
				</div>
			</InfiniteScroll>
		</div>
	);
};

export default PhotoSearch;