/** @jsxImportSource @emotion/react */
import React from 'react';
import './App.css';

import cx from "@emotion/css";

import Styles from './styles/Styles.js';

import PhotoSearch from "./components/PhotoSearch";

function App() {
	const Theme = new Styles();

	return (
		<div css={cx(Theme.Container, Theme.HFlex)} >
			<div css={Theme.VFlex}>
				<h1>React Photo Search</h1>
				<PhotoSearch Theme={Theme}/>
			</div>
		</div>
	);
}

export default App;